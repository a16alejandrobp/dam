# Programación de aplicaciones cliente

La programación de aplicaciones cliente y aplicaciones servidor que vamos a realizar, está basada en el uso de los protocolos estándar de la capa de aplicación, por tanto, cuando programemos una aplicación servidor basada en el protocolo HTTP, por ejemplo, diremos también que se está programando un servicio o servidor HTTP, y si lo que programamos es el cliente, hablaremos de cliente HTTP.

Nos vamos a centrar en la programación de aplicaciones cliente para los protocolos HTTP, FTP, SMTP y TELNET. Su programación se realizará
mediante bibliotecas especiales que proporcinan ciertos objetos que
facilitan la tarea de programación, y en algunos casos veremos también cómo programar esas mismas aplicaciones cliente mediante sockets.

Es imprescindible un mínimo conocimiento de cómo funciona el protocolo sobre el que vamos a construir un cliente, para tener claro el intercambio de mensajes que realiza con el servidor. Por tanto, no repares en volver a los primeros apartados o consultar los enlaces para saber más, donde se explica algo sobre el funcionamiento de estos protocolos. Esto es más necesario, si la programación se realiza a niveles relativamente bajos, como por ejemplo cuando utilizamos sockets.

## Programación de un cliente HTTP

Como ya te hemos comentado anteriormente, HTTP se basa en sencillas operaciones de solicitud/respuesta.

* Un cliente establece una conexión con un servidor y envía un mensaje con los datos de la solicitud.

* El servidor responde con un mensaje similar, que contiene el estado de la operación y su posible resultado.

![Ilustración Cliente-Servidor HTTP](img/ClienteServidorHTTP.png)

¿Es un cliente HTTP el ejemplo que hemos visto sobre el acceso a
recursos de la red mediante URL, utilizando las clases `URL` y `URLConnection`? Efectivamente, se trataba de un cliente web básico, en particular un cliente http muy básico, que no realiza por ejemplo la traducción de una página html tal y como lo hace un navegador.

Al programar aplicaciones con las clases `URL` y `URLConnection`, lo hacemos en un nivel alto, de manera que queda oculta toda la operatoria que era tan explícita al programar un cliente con sockets.

En el siguiente enlace puedes consultar otro ejemplo de cliente HTTP realizado con sockets y cuyo comportamiento es similar a los ejemplos vistos anteriormente mediante las clases URLs.

## Bibliotecas para programar un cliente FTP.

Java no dispone de bibliotecas específicas para programar clientes y servidores de FTP. Pero afortunadamente, la organización de software libre The Apache software Foundation proporciona una API para implementar clientes FTP en tus aplicaciones.

![Ilustración API Apache para trabajar con FTP](img/bibliotecaClienteFTP.png)

El **paquete de la API de Apache para trabajar con FTP** es org.apache.commons.net.ftp. Este paquete proporciona, entre otras, las siguientes clases:

* Clase `FTP`. Proporciona las funcionalidades básicas para
implementar un cliente FTP. Esta clase hereda de
SocketClient.

* Clase `FTPReplay`. Permite almacenar los valores devueltos por el servidor como código de respuesta a las peticiones del cliente.

* Clase `FTPClient`. Encapsula toda la funcionalidad que necesitamos para almacenar y recuperar archivos de un servidor FTP, encargándose de todos los detalles de bajo nivel para su interacción con el servidor. Esta clase hereda de SocketClient.

* Clase `FTPClientConfig`. Proporciona un forma alternativa de configurar objetos FTPClient.

* Clase `FTPSClient`. Proporciona FTP seguro, sobre SSL. Esta clase hereda de `FTPClient`.

En el siguiente recurso didáctico tienes los pasos a seguir para descargar la API de Apache y realizar su integración en NetBeans.

![Ilustración Portada Presentación Flash](doc/PSP05_CONT_R29_DescargaAPIdeApache/captura.png)
[SWF](doc/PSP05_CONT_R29_DescargaAPIdeApache/flash.swf) [HTML](PSP05_CONT_R29_DescargaAPIdeApache/flash.html)

En el siguiente [recurso adicional](http://commons.apache.org/proper/commons-net/apidocs/org/apache/commons/net/ftp/package-summary.html) dispones de toda la información sobre el API de Apache para el servicio FTP.

## Programación de un cliente FTP

Una forma sencilla de crear un cliente FTP es mediante la clase `FTPClient`.

![Ilustración FTP Client](img/claseFTPClient.png)

Una vez creado el cliente, como en cualquier objeto SocketClient habrá que seguir el siguente **esquema básico de trabajo**:

* **Realizar la conexión del cliente con el servidor**. El método `connect(InetAddress host)` realiza la conexión del cliente con el servidor de nombre host, abriendo un objeto Socket conectado al host remoto en el puerto por defecto.

* **Comprobar la conexión**. El método `getReplyCode()` devuelve un código de respuesta del servidor indicativo de el éxito o fracaso de la conexión.

* **Validar usuario**. El método login(String usuario, String password) permite esa validación. 

* **Realizar operaciones contra el servidor**. Por ejemplo:
	* Listar todos los ficheros disponibles en una determinada carpeta remota mediante el método `listNames()`.
	* Recuperar el contenido de un fichero remoto mediante `retrieveFile(String rutaRemota, OutputStream ficheroLocal)` y transferirlo al equipo local para escribirlo en el `ficheroLocal` especificado.

* **Desconexión del servidor**. El método `disconnect()` o `logout()` realiza la desconexión del servidor.

Ten en cuenta, que durante todo este proceso puede generarse tanto una SocketException si se superó el tiempo de espera para conectar con el servidor, o una `IOException` si no se tiene acceso al fichero especificado.

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-ejemplosserviciosdered/src/master/src/main/java/psp/SampleFTPClient.java), tienes un ejemplo de programación de un cliente FTP, que se conecta a un servidor remoto y se descarga un fichero. Ten en cuenta que para poder ejecutarlo, puede que tengas que desactivar cualquier cortafuegos o firewall que tengas activo.

## Programación de un cliente Telnet

Telnet (*TELecommunication NETwork*) es un **protocolo que permite acceder a otro equipo de la red y adminstrarlo de forma remota, esto es, como si estuviéramos sentados delante de él.**

El protocolo Telenet está basado en:

* El **modelo cliente/servidor**, por lo que su esquema básico de funcionamiento será el típico de esta arquitectura.
* El **servidor escucha** las peticiones por el **puerto** `23`.
* Su funcionamiento es en **modo texto**.

Este servicio puede resultar muy útil para administrar por ejemplo equipos sin pantalla o teclado, o bien servidores apilados en un rack o que no estén físicamente presentes.

En la actualidad, no se suele utilizar, debido a la poca seguridad que ofrece, ya que toda la información que se intercambia entre servidor y cliente, incluidos usuario y contraseña, se transmiten en texto plano. Esto es así, porque Telnet se creó pensando en la facilidad de uso y no en la seguridad. En su lugar, se utiliza un servicio de acceso y control remoto basado en el protocolo SSH.

Veremos un ejemplo de **programación de un cliente Telnet, sólo a modo ilustrativo de uso de otra biblioteca proporcionada por el API de Apache**, `org.apache.commons.net`, que ya te descargaste en los apartados anteriores.

La biblioteca necesaria es `org.apache.commons.net.telnet`. Para disponer de ella, tendremos que agregar a las bibliotecas del proyecto, como en el ejemplo de FTP, el archivo `commons-net-n.x.x.jar`. Entre las clases que proporciona para programar un cliente Telnet está:

* Clase **TelnetClient**. Permite implementar un terminal virtual para el protocolo Telnet. Hereda de la clase `SocketClient`.

	* El método `SocketClient.connect()` realiza la conexión al servidor.
	* Los métodos `TelnetClient.getInputStream()` y `TelnetClient.getOutputStream()` permiten a través de objetos `InputStream()` y `OutputStream()` enviar y recibir datos a través de la conexión Telnet.
	* El método `TelnetClient.disconnect()` cierra la conexión al servidor, así como los flujos de entrada y salida y el socket abiertos.

En el siguiente [recurso adicional](http://commons.apache.org/proper/commons-net/apidocs/org/apache/commons/net/telnet/package-summary.html) dispones de toda la información sobre la biblioteca del API de Apache para el servicio Telnet. Puedes ampliar con el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Secure_Shell) alternativas a Telnet como es **SSH**.

## Programación de un cliente SMTP

Utilizaremos el `API javax.mail`. Este paquete proporciona las clases necesarias para implementar un sistema de correo. Vamos a destacar **las clases y métodos del paquete `javax.mail` que nos permitirán crear nuestro cliente de correo**:

* **Clase `Session`**. Representa una sesión de correo. Agrupa las propiedades y valores por defecto que utiliza el API `javax.mail` para el correo.
Método `getDefaultInstance()`. Obtiene la sesión por defecto. Si no ha sido configurada, se creará una nueva de manera predeterminada. El parámetro que se le pasa debe recoger al menos las siguientes propiedades: protocolo y servidor `smtp`, puerto para el socket de sesión y tipo, usuario y puerto `smtp`).

* **Clase `Message`**. Modela un mensaje de correo electrónico.

	* Método `setFrom()`. Asigna el atributo From al mensaje, siendo éste la dirección del emisor. Método setRecipients(). Asigna el tipo y direcciones de destinatarios.
	* Método `setSubject()`. Para indicar asunto del mensaje.
	* Método `setText()`. Asigna el texto o cuerpo del mensaje.

* **Clase `Transport`**. Representa el transporte de mensajes. Hereda de la clase `Service`, la cual proporciona funcionalidades comunes a todos los servicios de mensajería, tales como conexión, desconexión, transporte y almacenamiento.

	* Método `send()`. Realiza el envío del mensaje a todas las direcciones indicadas. Si alguna dirección de destino no es válida, se lanza una excepción `SendFailedException`.

En la siguiente presentación te indicamos los pasos a seguir para descargar el API `javax.mail` e integrarla en NetBeans.

![ilustración Portada presentación](doc/PSP05_CONT_R34_DescargaAPIjavaxMail/captura.png)
[SWF](doc/PSP05_CONT_R34_DescargaAPIjavaxMail/flash.swf) [HTML](doc/PSP05_CONT_R34_DescargaAPIjavaxMail/flash.html)

Y en el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-ejemplosserviciosdered/src/master/src/main/java/psp/SampleEnviarCorreoSSL.java) dispones del código java completo. Observa que no se realiza de forma explícita una conexión y desconexión, esto es debido a que va implícito en el objeto `Transport`. Cuando ejecutes el programa, recuerda poner los datos reales de tu cuenta de Gmail (o bien crea una) así como un destinatario válido (puede ser tu misma cuenta de Gmail). Observa también que el protocolo SMTP no utiliza el puerto `25`, esto es porque se trata de *SMTP seguro* (SMTP sobre SSL).

Utiliza el siguiente [recurso adicional](https://docs.oracle.com/javaee/7/api/javax/mail/package-summary.html) para conocer más posibilidades de programación de sistemas de correo con el API `javax.mail`.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)