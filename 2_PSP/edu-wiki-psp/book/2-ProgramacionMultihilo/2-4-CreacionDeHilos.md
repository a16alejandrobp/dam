# Creación de Hilos

En Java, un hilo se representa mediante una instancia de la clase `java.lang.Thread`. Este objeto `Thread` se emplea para iniciar, detener o cancelar la ejecución del hilo de ejecución.

Los hilos o threads se pueden **implementar o definir de dos formas**:

* Extendiendo la clase `Thread`. 
* Mediante la interfaz `Runnable` .

En ambos casos, se debe proporcionar una definición del método `run()`, ya que este método es el que contiene el código que ejecutará el hilo, es decir, su comportamiento.

El procedimiento de construcción de un hilo es independiente de su uso, pues una vez creado se emplea de la misma forma. Entonces, ¿cuando utilizar uno u otro procedimiento?

* Extender la clase `Thread` es el procedimiento más sencillo, pero no siempre es posible. Si la clase ya hereda de alguna otra clase padre, no será posible heredar también de la clase `Thread` (recuerda que Java no permite la herencia múltiple), por lo que habrá que recurrir al otro procedimiento.

* Implementar `Runnable` siempre es posible, es el procedimiento más general y también el más flexible.

![](img/herenciaHilo.png)

Por ejemplo, piensa en la programación de `applets`, cualquiera de ellos tiene que heredar de la clase `java.applet.Applet` y en consecuencia ya no puede heredar de `Thread` si se quiere utilizar hilos. En este caso, no queda más remedio que crear los hilos implementando `Runnable`.

Cuando la Máquina Virtual Java (JVM) arranca la ejecución de un programa, ya hay un hilo ejecutándose, denominado hilo principal del programa, controlado por el método `main()`, que se ejecuta cuando comienza el programa y es el último hilo que termina su ejecución, ya que cuando este hilo finaliza, el programa termina.

El siguiente [ejemplo](doc/PSP02_CONT_R14_HiloPrincipal/HiloPrincipal/src/Main.java) muestra lo que te acabamos de comentar, siempre hay un hilo que ejecuta el método `main()`, y por defecto, este hilo se llama `main`. Observa que para saber qué hilo se está ejecutando en un momento dado, el hilo en curso, utilizamos el método `currentThread()` y que obtenemos su nombre invocando al método `getName()`, ambos de la clase `Thread`.

## Creación de hilos extendiendo la clase `Thread`

Para definir y crear un hilo extendiendo la clase `Thread`, haremos lo siguiente:

* Crear una nueva clase que herede de la clase `Thread`.

* Redefinir en la nueva clase el método `run()` con el código asociado al hilo. Las sentencias que ejecutará el hilo.

* Crear un objeto de la nueva clase `Thread`. Éste será realmente el hilo.

Una vez creado el hilo, para ponerlo en marcha o iniciarlo:

* Invocar al método `start()` del objeto `Thread` (el hilo que hemos creado).

En el siguiente [ejemplo](doc/PSP02_CONT_R18_crear_hilo_Thread/crear_hilo_Thread/src/Saludo.java) puedes ver los pasos indicados anteriormente para la creación de un hilo extendiendo la clase `Thread`. El hilo que se crea (objeto `Thread` hilo1) imprime un mensaje de saludo. Para simplificar el ejemplo se ha incluido el método `main()` que inicia el programa en la propia clase `Saludo`.

```java
 public class Saludo extends Thread {
 //clase  que extiende a Thread
    @Override
    public void run() {
    // se redefine el método run() con el código asociado al hilo
        System.out.println("¡Saludo desde un hilo extendiendo thread!");
    }
    public static void main(String args[]) {
        Saludo hilo1=new Saludo();
        //se crea un objeto Thread, el hilo hilo1
        hilo1.start();
        //invoca a start() y pone en marcha el hilo hilo1
    }
  }
```

## Creación de hilos mediante la interfaz `Runnable`.

Para **definir y crear hilos implementando la interfaz `Runnable`** seguiremos los siguientes pasos:

* Declarar una nueva clase que implemente a `Runnable`.

* Redefinir (o sombrear) en la nueva clase el método `run()` con el código asociado al hilo. Lo que queremos que haga el hilo.

* Crear un objeto de la nueva clase.

* Crear un objeto de la clase `Thread` pasando como argumento al constructor, el objeto cuya clase tiene el método `run()`. Este será realmente el hilo.

Una vez creado el hilo, para ponerlo en marcha o iniciarlo:

* Invocar al método `start()` del objeto `Thread` (el hilo que hemos creado).

El siguiente [ejemplo](doc/PSP02_CONT_22_crea_hilo_Runnable/crea_hilo_Runnable/src/Saludo.java) muestra cómo crear un hilo implementado Runnable. El hilo que se crea (objeto thread hilo1) imprime un mensaje de saludo, como en el caso anterior.

```java
public class Saludo implements Runnable {
//clase que implementa a Runnable
    public void run() {
    //se redefine el método run()con el código asociado al hilo
        System.out.println("¡Saludo desde un hilo creado con Runnable!");
    }
    public static void main(String args[]) {
       Saludo  miRunnable=new Saludo();
       //se crea un objeto  Saludo
       Thread hilo1= new Thread(miRunnable);
       //se crea un objeto Thread (el hilo hilo1) pasando como argumento
      // al constructor un objeto Saludo
        hilo1.start();
       //se invoca al método start() del hilo hilo1
    }
}
```

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)