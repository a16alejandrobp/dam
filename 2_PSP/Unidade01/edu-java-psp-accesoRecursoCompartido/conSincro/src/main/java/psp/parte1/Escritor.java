package psp.parte1;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;

/**
 *
 * @author usuario
 */
public class Escritor {

    /**
     * <p>
     * Aplicación que accede a un fichero que contiene un valor entero. Lee ese
     * valor, lo incrementa en 1 y escribe ese valor actualizado en el mismo
     * fichero.
     * </p>
     *
     * <code>java -jar escritor 1 prueba.txt 3</code>
     *
     * @param args 1ª pos: IdProceso o Número de orden de creación del proceso
     * @param args 2ª pos: Path del recurso o fichero a crear para incrementar los
     *             valores
     * @param args 3ª pos: Número de incrementos a realizar el el fichero
     */
    public static void main(String[] args) {
        int orden = 0;
        String nombreFichero = "";
        File archivo = null;
        RandomAccessFile raf = null;
        FileLock bloqueo = null;
        int valor = 0;
        int numeroDeIncrementos = 1; // Número de veces que incrementará el nº que encuentre en el fichero (1
                                     // default)
        // Comprobamos si estamos recibiendo argumentos en la línea de comandos
        if (args.length > 0) {
            orden = Integer.parseInt(args[0]);
            // Número de orden de creación de este proceso
            try {
                // Rediregimos salida y error estándar a un fichero
                PrintStream ps = new PrintStream(
                        new BufferedOutputStream(new FileOutputStream(new File("log-escritores.txt"), true)), true);
                System.setOut(ps);
                System.setErr(ps);
            } catch (Exception e) {
                System.err.println("P" + orden + " No he podido redirigir salidas.");
            }
        }
        // Identificamos el sistema operativo para poder acceder por su ruta al
        // fichero de forma correcta.
        String osName = System.getProperty("os.name");
        if (osName.toUpperCase().contains("WIN")) { // Windows
            if (args.length > 1)
                nombreFichero = args[1].replace("\\", "\\\\");
            // Hemos recibido la ruta del fichero en la línea de comandos
            else {
                nombreFichero = "C:\\file_escritores";
                // Fichero que se utilizará por defecto
            }
        } else { // GNU/Linux
            if (args.length > 1)
                nombreFichero = args[1];
            // Hemos recibido la ruta del fichero en la línea de comandos
            else {
                nombreFichero = "file_escritores";
                // Fichero que se utilizará por defecto
            }
        }
        if (args.length > 2) {
            numeroDeIncrementos = Integer.parseInt(args[2]);
            if (numeroDeIncrementos < 0) {
                System.err.println("P" + orden + " No puede ser un número negativo los incrementos al realizar");
                System.exit(1);
            }
        }
        // Preparamos el acceso al fichero
        archivo = new File(nombreFichero);
        if (!archivo.exists()) {
            archivo = crearFichero(nombreFichero, orden);
        }
        for (int i = 0; i < numeroDeIncrementos; i++)// aumentamos las situaciones de concurrencia
            try {
                raf = new RandomAccessFile(archivo, "rwd"); // Abrimos el fichero
                // ***************
                // Sección crítica
                bloqueo = raf.getChannel().lock();
                // bloqueamos el canal de acceso al fichero. Obtenemos el objeto que
                // representa el bloqueo para después poder liberarlo
                System.out.println("Proceso" + orden + ": ENTRA sección");
                // Lectura del fichero
                valor = raf.readInt(); // leemos el valor
                valor++; // incrementamos
                raf.seek(0); // volvemos a colocarnos al principio del fichero
                raf.writeInt(valor); // escribimos el valor
                System.out.println("Proceso" + orden + ": SALE sección");
                bloqueo.release(); // Liberamos el bloqueo del canal del fichero
                bloqueo = null;
                // Fin sección crítica
                // *******************
                System.out.println("Proceso" + orden + ": valor escrito " + valor);
            } catch (Exception e) {
                System.err.println("P" + orden + " Error al acceder al fichero");
                System.err.println(e.toString());
            } finally {
                try {
                    if (null != raf)
                        raf.close();
                    if (null != bloqueo)
                        bloqueo.release();
                } catch (Exception e2) {
                    System.err.println("P" + orden + " Error al cerrar el fichero");
                    System.err.println(e2.toString());
                    System.exit(1); // Si hay error, finalizamos
                }
            }

    }

    private static File crearFichero(String pPath, int pOrden) {
        File newArchivo = new File(pPath);
        RandomAccessFile rf = null;
        try {
            newArchivo.createNewFile(); // if file already exists will do nothing
            rf = new RandomAccessFile(newArchivo, "rwd");
            rf.writeInt(0);
        } catch (IOException e) {
            System.err.println("P" + pOrden + " No fue posible crear el nuevo fichero");
            System.exit(1);
        } finally {
            try {
                if (null != rf)
                    rf.close();
            } catch (Exception e2) {
                System.err.println("P" + pOrden + " Error al cerrar el nuevo fichero");
                System.err.println(e2.toString());
                System.exit(1); // Si hay error, finalizamos
            }
        }
        return newArchivo;
    }

}
