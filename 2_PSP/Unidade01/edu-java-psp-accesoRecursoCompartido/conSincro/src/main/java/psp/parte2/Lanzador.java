package psp.parte2;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

@SuppressWarnings("all")
public class Lanzador {

    /**
     * Lanzará un nº de procesos del programa escritor. Pasará la ruta del archivo y
     * el nº de incrementos que debe realizar cada proceso del programa escritor.
     * Sin ningún parámetro se utilizan valores default.
     *
     * Ejemplo: <code>java -jar lanzador prueba.txt 1 2</code>
     *
     * @param args 1ª pos: Ruta del archivo donde almacenará los incrementos.
     *             Generado en el patch actual. (No se puede especificar un path)
     * @param args 2ª pos: Número de incrementos que debe hacer cada proceso
     * @param args 3ª pos: Número de procesos a lanzar
     */
    public static void main(String[] args) {
        Process nuevoProceso; // Definimos una variable de tipo Process
        String nombreFichero;
        int numeroDeIncrementos = 1; // Nº de veces que incrementará el nº que encuentre en el fichero (1 default)
        int numeroDeProcesos = 20; // Número de procesos del programa "Escritor" que debe lanzar (20 default)
        File archivo = null;
        RandomAccessFile raf = null;

        // Identificamos el sistema operativo para poder acceder por su ruta al
        // fichero de forma correcta.
        String osName = System.getProperty("os.name");
        if (osName.toUpperCase().contains("WIN")) { // Windows
            if (args.length > 0)
                nombreFichero = args[0].replace("\\", "\\\\");
            // Hemos recibido la ruta del fichero en la línea de comandos
            else {
                nombreFichero = "C:\\file_escritores";
                // Fichero que se utilizará por defecto
            }
        } else { // GNU/Linux
            if (args.length > 0) {
                // Hemos recibido la ruta del fichero en la línea de comandos
                nombreFichero = "file_escritores_" + args[0];
                // Hemos recibido el numero de incrementos que debe hacer
                numeroDeIncrementos = Integer.parseInt(args[1]);
                // Hemos recibido el numero de procesos que debe lanzar
                numeroDeProcesos = Integer.parseInt(args[2]);
            } else {
                nombreFichero = "file_escritores";
                // Fichero que se utilizará por defecto
            }
        }
        try {
            // Redirigimos salida estándar y de error a un fichero
            PrintStream ps = new PrintStream(
                    new BufferedOutputStream(new FileOutputStream(new File("log-lanzadores.txt"), true)), true);
            System.setOut(ps);
            System.setErr(ps);
        } catch (Exception e) {
            System.err.println("Error al redirigir las salidas");
            System.err.println(e.toString());
        }

        archivo = new File(nombreFichero);
        // Preparamos el acceso al fichero
        if (!archivo.exists()) {
            // Si no existe el fichero
            try {
                archivo.createNewFile(); // Lo creamos
                raf = new RandomAccessFile(archivo, "rw"); // Abrimos el fichero
                // El modo tiene que ser lectura-escritura. No es posible sólo escritura
                raf.writeInt(0); // Escribimos el valor inicial 0
                System.out.println("Creado el fichero.");
            } catch (Exception e) {
                System.err.println("Error al crear el fichero");
                System.err.println(e.toString());
            } finally {
                try { // Nos asegurarnos que se cierra el fichero.
                    if (null != raf)
                        raf.close();
                } catch (Exception e2) {
                    System.err.println("Error al cerrar el fichero");
                    System.err.println(e2.toString());
                    System.exit(1); // Si hay error, finalizamos
                }
            }
        }
        // Creamos un grupo de procesos que accederán al mismo fichero
        try {
            for (int idProceso = 1; idProceso <= numeroDeProcesos; idProceso++) {
                nuevoProceso = Runtime.getRuntime()
                        .exec("java -jar " + "escritor " + idProceso + " " + nombreFichero + " " + numeroDeIncrementos);
                // Creamos el nuevo proceso y le indicamos el número de orden y
                // el fichero que debe utilizar.
                System.out.println("Creado el proceso " + idProceso);
                // Mostramos en consola que hemos creado otro proceso
            }
        } catch (SecurityException ex) {
            System.err.println(
                    "Ha ocurrido un error de Seguridad." + "No se ha podido crear el proceso por falta de permisos.");
        } catch (Exception ex) {
            System.err.println("Ha ocurrido un error, descripción: " + ex.toString());
        }
    }

}
